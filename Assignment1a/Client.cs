﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a
{
    public class Client
    {
        private string clientFirstName;
        private string clientLastName;
        private string clientEmail;
        private string clientAddress;
        private string clientPhone;
        private string clientLicence;

        public Client()
        {

        }

        public string ClientFirstName
        {
            get { return clientFirstName; }
            set { clientFirstName = value; }
        }

        public string ClientLastName
        {
            get { return clientLastName; }
            set { clientLastName = value; }
        }



        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }


        public string ClientAddress
        {
            get { return clientAddress; }
            set { clientAddress = value; }
        }

        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }


        public string ClientLicence
        {
            get { return clientLicence; }
            set { clientLicence = value; }
        }
    }

}
/**********************************************************
 * This code has elements modified and adapted  from Christine Bittle's source code
 * 
 * *********************************************************/
