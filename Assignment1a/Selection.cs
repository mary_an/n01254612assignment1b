﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a
{
    public class Selection
    {
        //declare variables 
        Vehicle vehicle;
        PickUp pickup;
        Client client;

        public Selection(Vehicle v, PickUp p, Client c)
        {
            vehicle = v;
            pickup = p;
            client = c;
        }

        public string PrintSelection()
        {
            string selection = "Final Selection:<br />";

            selection += "First Name: " + client.ClientFirstName + "<br />";
            selection += "Last Name: " + client.ClientLastName + "<br/>";

            selection += "Email: " + client.ClientEmail + "<br />";
            selection += "Address: " + client.ClientAddress + "<br />";
           selection += "Phone Number: " + client.ClientPhone + "<br />";
            selection += "Number of Driver's Licence: " + client.ClientLicence + "<br />";

            selection += "Type of Vehicle: " + vehicle.type + "<br />";


            selection += "Usage Period: " + vehicle.period.ToString() +" days " + "<br />";
            selection += "Features: " + String.Join(" , ", vehicle.features.ToArray()) + "<br />";
            selection += "Pick-Up information: " +"Pick-Up Location: " + pickup.pickuplocation + ", pickup time: " + pickup.time + ", pickup day: " + pickup.pickupday + "<br />";
            selection += "Total :" + CalculateSelection().ToString() + "$" + "<br />";


            return selection;
        }

        public double CalculateSelection()
        {
            double total = 0;
            int rate = 0;

            if (vehicle.type == "Crossover")
            {
                rate = 79;
                total = rate * vehicle.period;
            }
            else if (vehicle.type == "Compact")
            {
                rate = 59;
                total = rate * vehicle.period;
            }
            else if (vehicle.type == "Hybrid")
            {
                rate = 69;
                total = rate*vehicle.period;
            }
            System.Diagnostics.Debug.WriteLine("total " + total);
            System.Diagnostics.Debug.WriteLine("rate " + rate);
            System.Diagnostics.Debug.WriteLine("vehicle period: " + vehicle.period);
            System.Diagnostics.Debug.WriteLine(vehicle.features.Count());

            total += vehicle.features.Count() * 20;

            return total;
        }


    }
}/**********************************************************
 * This code has elements modified and adapted  from Christine Bittle's source code
 * 
 * *********************************************************/
