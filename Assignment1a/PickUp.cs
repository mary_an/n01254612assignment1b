﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a
{
    public class PickUp
    {
        public string pickuplocation;
        public string time;
        public string pickupday;

        public PickUp(string l, string t, string d)
        {
            pickuplocation = l;
            time = t;
            pickupday = d;
        }

    }
}
/**********************************************************
 * This code has elements modified and adapted  from Christine Bittle's source code
 * 
 * *********************************************************/
