﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1a.aspx.cs" Inherits="Assignment1a.Assignment1a" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Car Sharing Service | Moonlight </title>
    <link href="Style.css" rel="stylesheet" />
</head>

<body>
    <form id="form1" runat="server">
        <h3 runat="server" id="text"></h3>

            <h1>MoonlighT</h1> 
            <p>CAR SHARING SERVICE</p>
            <br />
            <asp:validationsummary forecolor="Red" runat="server" id="validationSummary">
                </asp:validationsummary>
            <h5>PLACE YOUR ORDER</h5>
        
           
            <div class="fname">
                <asp:Label Text="First Name:" runat="server" /> <br />
                <asp:TextBox runat="server" ID="clientfName" placeholder="First Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your First Name" ControlToValidate="clientfName" ID="validatorfName"></asp:RequiredFieldValidator>
            </div>
           
            <div class="lname">
                <asp:Label Text="Last Name:" runat="server" /> <br />
                <asp:TextBox runat="server" ID="clientlName" placeholder="Last Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Last Name" ControlToValidate="clientlName" ID="validatorlName"></asp:RequiredFieldValidator>
            </div>
            
            <div>
                <asp:Label Text="Email:" runat="server" /> <br />
                <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            </div>
            
            
          
            <div>
                <asp:Label Text="Address:" runat="server" /> <br />
                <asp:TextBox runat="server" ID="clientAddress" placeholder="e.g. 22 Prince Edward Dr "></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Address" ControlToValidate="clientAddress" ></asp:RequiredFieldValidator>
            </div>
            
           
            
            <div>
                <asp:Label Text="Phone Number:" runat="server" /> <br />
                <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" 
                        runat="server" ErrorMessage="This is an invalid phone number" 
                        ControlToValidate ="clientPhone" 
                        ValidationExpression= "^([0-9\(\)\/\+ \-]*)$"></asp:RegularExpressionValidator>
           
            </div>
         <br />
            <div>
                <asp:Label Text="Number of Driver's Licence:" runat="server" /> <br />
                <asp:TextBox runat="server" ID="clientLicence" placeholder="A1234-12345-12345"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter the number of your Driver's Licence" ControlToValidate="clientLicence" ID="validatorLicence"></asp:RequiredFieldValidator>
            </div>   
        <br />   
               
             <label>Vehicles:</label>
                <br />
                <asp:RadioButtonList runat="server" ID="vehicleType">
                <asp:ListItem Value="Crossover" Text="Crossover" >Crossover | SUV</asp:ListItem>
                <asp:ListItem Value="Compact" Text="Compact" >Compact | Sedan</asp:ListItem>
                <asp:ListItem Value="Hybrid" Text="Hybrid" >Hybrid | Electric</asp:ListItem>
                </asp:RadioButtonList>
                 
        <br />
               <div>
            <label>Usage Period (days):</label><br />
                <asp:TextBox runat="server" ID="usageTime" placeholder="Enter the number of days"></asp:TextBox>
                <asp:RangeValidator runat="server" ControlToValidate="usageTime" Type="Integer" MinimumValue="1" MaximumValue="7" ErrorMessage="Enter a valid number (between 1 and 7)"></asp:RangeValidator>
              </div>
        <br />
          
           <fieldset >
               <legend><b>Pick-up information</b></legend>   
                <asp:Label Text="Location:" runat="server" /> <br />
                    <asp:DropDownList runat="server" ID="pickupLocation">
                    <asp:ListItem Value="Select" Text="-Select-"></asp:ListItem>
                    <asp:ListItem Value="Toronto | 2540 Dufferin St" Text="Toronto | 2540 Dufferin St"></asp:ListItem>
                    <asp:ListItem Value="Mississauga | 307 Dundas St East" Text="Mississauga | 307 Dundas St East"></asp:ListItem>
                    <asp:ListItem Value="Lake Shore | 3577 Lake Shore Blvd W" Text="Lake Shore | 3577 Lake Shore Blvd W"></asp:ListItem>
               </asp:DropDownList>
               <br />
           
            <asp:Label Text="Time:" runat="server" /> <br />
            <asp:TextBox runat="server" ID="pickupTime" placeholder="24 hour format e.g 1000"></asp:TextBox>
            <asp:RangeValidator runat="server" Type="Integer" 
            MinimumValue ="1000" MaximumValue="1900" ControlToValidate="pickupTime" 
            ErrorMessage="Our hours are between 10am and 7pm" />       
          <br />
               <asp:Label Text="Day:" runat="server" /> <br />
                    <asp:DropDownList runat="server" ID="pickupDay">
                    
                    <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                    <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                    <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                    <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                    <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
               </asp:DropDownList>
               <br />
           </fieldset>
        <br />
          
        <div ID="vehicle_features" runat="server">
                <asp:Label Text="Features:" runat="server" /> <br />
                <asp:CheckBox runat="server" ID="carFeature1" Text="Heated Seats" />
                <asp:CheckBox runat="server" ID="carFeature2" Text="Heated Steering Wheel" />
                <asp:CheckBox runat="server" ID="carFeature3" Text="Adaptive Cruise Control (ACC)" />
                <asp:CheckBox runat="server" ID="carFeature4" Text="Panoramic Sunroof" />
                <asp:CheckBox runat="server" ID="carFeature5" Text="Blind-spot Detector" />
          </div>
        <br />    
           
        <div class="button">
                <asp:Button  runat="server" ID="myButton" OnClick="myButton_Click" Text="Submit"/>
        </div>
    </form>
    <div id="info" runat="server">

    </div>
</body>
</html>
