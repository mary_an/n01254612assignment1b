﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1a
{
    public class Vehicle
    {
        public List<string> features;
        public int period;
        public string type;
        
        public Vehicle(List<string> f, int p, string t)
        {
            features = f;
            period = p;
            type = t;
        }
    }
}
/**********************************************************
 * This code has elements modified and adapted  from Christine Bittle's source code
 * 
 * *********************************************************/
