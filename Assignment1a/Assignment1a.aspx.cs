﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1a
{
    public partial class Assignment1a : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
       {
           


        }

        protected void myButton_Click(object sender, EventArgs e)
        {
          string type = vehicleType.SelectedItem.Value.ToString();
          string day = usageTime.Text.ToString();
          int period = int.Parse(day);
            List<string> vehiclefeatures = new List<string>();

            foreach (Control control in vehicle_features.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox feature = (CheckBox)control;
                    if (feature.Checked)
                    {
                        vehiclefeatures.Add(feature.Text);
                    }

                }
            }

           // newvehicle.features = newvehicle.features.Concat(vehiclefeatures).ToList();


            Vehicle newvehicle = new Vehicle(vehiclefeatures, period, type);


            string firstName = clientfName.Text.ToString();
            string lastName = clientlName.Text.ToString();
            string email = clientEmail.Text.ToString();
            string address = clientAddress.Text.ToString();
            string phone = clientPhone.Text.ToString();
            string licence = clientLicence.Text.ToString();
            Client newclient = new Client();
            newclient.ClientFirstName = firstName;
            newclient.ClientLastName = lastName;
            newclient.ClientEmail = email;
            newclient.ClientAddress = address;
            newclient.ClientPhone = phone;
            newclient.ClientLicence = licence;


            string pickuplocation = pickupLocation.SelectedItem.Value.ToString();
            string time = pickupTime.Text.ToString();
            string pickupday = pickupDay.SelectedItem.Value.ToString();
            PickUp newpickup = new PickUp(pickuplocation, time, pickupday);
        
            Selection newSelection = new Selection(newvehicle, newpickup, newclient);

            info.InnerHtml = newSelection.PrintSelection();





/**********************************************************
 * This code has elements modified and adapted  from Christine Bittle's source code
 * 
 * *********************************************************/



        }

    }
    

}
